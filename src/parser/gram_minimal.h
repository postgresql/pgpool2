/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_MINIMAL_BASE_YY_GRAM_MINIMAL_H_INCLUDED
# define YY_MINIMAL_BASE_YY_GRAM_MINIMAL_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int minimal_base_yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    IDENT = 258,
    UIDENT = 259,
    FCONST = 260,
    SCONST = 261,
    USCONST = 262,
    BCONST = 263,
    XCONST = 264,
    Op = 265,
    ICONST = 266,
    PARAM = 267,
    TYPECAST = 268,
    DOT_DOT = 269,
    COLON_EQUALS = 270,
    EQUALS_GREATER = 271,
    LESS_EQUALS = 272,
    GREATER_EQUALS = 273,
    NOT_EQUALS = 274,
    ABORT_P = 275,
    ABSENT = 276,
    ABSOLUTE_P = 277,
    ACCESS = 278,
    ACTION = 279,
    ADD_P = 280,
    ADMIN = 281,
    AFTER = 282,
    AGGREGATE = 283,
    ALL = 284,
    ALSO = 285,
    ALTER = 286,
    ALWAYS = 287,
    ANALYSE = 288,
    ANALYZE = 289,
    AND = 290,
    ANY = 291,
    ARRAY = 292,
    AS = 293,
    ASC = 294,
    ASENSITIVE = 295,
    ASSERTION = 296,
    ASSIGNMENT = 297,
    ASYMMETRIC = 298,
    ATOMIC = 299,
    AT = 300,
    ATTACH = 301,
    ATTRIBUTE = 302,
    AUTHORIZATION = 303,
    BACKWARD = 304,
    BEFORE = 305,
    BEGIN_P = 306,
    BETWEEN = 307,
    BIGINT = 308,
    BINARY = 309,
    BIT = 310,
    BOOLEAN_P = 311,
    BOTH = 312,
    BREADTH = 313,
    BY = 314,
    CACHE = 315,
    CALL = 316,
    CALLED = 317,
    CASCADE = 318,
    CASCADED = 319,
    CASE = 320,
    CAST = 321,
    CATALOG_P = 322,
    CHAIN = 323,
    CHAR_P = 324,
    CHARACTER = 325,
    CHARACTERISTICS = 326,
    CHECK = 327,
    CHECKPOINT = 328,
    CLASS = 329,
    CLOSE = 330,
    CLUSTER = 331,
    COALESCE = 332,
    COLLATE = 333,
    COLLATION = 334,
    COLUMN = 335,
    COLUMNS = 336,
    COMMENT = 337,
    COMMENTS = 338,
    COMMIT = 339,
    COMMITTED = 340,
    COMPRESSION = 341,
    CONCURRENTLY = 342,
    CONDITIONAL = 343,
    CONFIGURATION = 344,
    CONFLICT = 345,
    CONNECTION = 346,
    CONSTRAINT = 347,
    CONSTRAINTS = 348,
    CONTENT_P = 349,
    CONTINUE_P = 350,
    CONVERSION_P = 351,
    COPY = 352,
    COST = 353,
    CREATE = 354,
    CROSS = 355,
    CSV = 356,
    CUBE = 357,
    CURRENT_P = 358,
    CURRENT_CATALOG = 359,
    CURRENT_DATE = 360,
    CURRENT_ROLE = 361,
    CURRENT_SCHEMA = 362,
    CURRENT_TIME = 363,
    CURRENT_TIMESTAMP = 364,
    CURRENT_USER = 365,
    CURSOR = 366,
    CYCLE = 367,
    DATA_P = 368,
    DATABASE = 369,
    DAY_P = 370,
    DEALLOCATE = 371,
    DEC = 372,
    DECIMAL_P = 373,
    DECLARE = 374,
    DEFAULT = 375,
    DEFAULTS = 376,
    DEFERRABLE = 377,
    DEFERRED = 378,
    DEFINER = 379,
    DELETE_P = 380,
    DELIMITER = 381,
    DELIMITERS = 382,
    DEPENDS = 383,
    DEPTH = 384,
    DESC = 385,
    DETACH = 386,
    DICTIONARY = 387,
    DISABLE_P = 388,
    DISCARD = 389,
    DISTINCT = 390,
    DO = 391,
    DOCUMENT_P = 392,
    DOMAIN_P = 393,
    DOUBLE_P = 394,
    DROP = 395,
    EACH = 396,
    ELSE = 397,
    EMPTY_P = 398,
    ENABLE_P = 399,
    ENCODING = 400,
    ENCRYPTED = 401,
    END_P = 402,
    ENUM_P = 403,
    ERROR_P = 404,
    ESCAPE = 405,
    EVENT = 406,
    EXCEPT = 407,
    EXCLUDE = 408,
    EXCLUDING = 409,
    EXCLUSIVE = 410,
    EXECUTE = 411,
    EXISTS = 412,
    EXPLAIN = 413,
    EXPRESSION = 414,
    EXTENSION = 415,
    EXTERNAL = 416,
    EXTRACT = 417,
    FALSE_P = 418,
    FAMILY = 419,
    FETCH = 420,
    FILTER = 421,
    FINALIZE = 422,
    FIRST_P = 423,
    FLOAT_P = 424,
    FOLLOWING = 425,
    FOR = 426,
    FORCE = 427,
    FOREIGN = 428,
    FORMAT = 429,
    FORWARD = 430,
    FREEZE = 431,
    FROM = 432,
    FULL = 433,
    FUNCTION = 434,
    FUNCTIONS = 435,
    GENERATED = 436,
    GLOBAL = 437,
    GRANT = 438,
    GRANTED = 439,
    GREATEST = 440,
    GROUP_P = 441,
    GROUPING = 442,
    GROUPS = 443,
    HANDLER = 444,
    HAVING = 445,
    HEADER_P = 446,
    HOLD = 447,
    HOUR_P = 448,
    IDENTITY_P = 449,
    IF_P = 450,
    ILIKE = 451,
    IMMEDIATE = 452,
    IMMUTABLE = 453,
    IMPLICIT_P = 454,
    IMPORT_P = 455,
    IN_P = 456,
    INCLUDE = 457,
    INCLUDING = 458,
    INCREMENT = 459,
    INDENT = 460,
    INDEX = 461,
    INDEXES = 462,
    INHERIT = 463,
    INHERITS = 464,
    INITIALLY = 465,
    INLINE_P = 466,
    INNER_P = 467,
    INOUT = 468,
    INPUT_P = 469,
    INSENSITIVE = 470,
    INSERT = 471,
    INSTEAD = 472,
    INT_P = 473,
    INTEGER = 474,
    INTERSECT = 475,
    INTERVAL = 476,
    INTO = 477,
    INVOKER = 478,
    IS = 479,
    ISNULL = 480,
    ISOLATION = 481,
    JOIN = 482,
    JSON = 483,
    JSON_ARRAY = 484,
    JSON_ARRAYAGG = 485,
    JSON_EXISTS = 486,
    JSON_OBJECT = 487,
    JSON_OBJECTAGG = 488,
    JSON_QUERY = 489,
    JSON_SCALAR = 490,
    JSON_SERIALIZE = 491,
    JSON_TABLE = 492,
    JSON_VALUE = 493,
    KEEP = 494,
    KEY = 495,
    KEYS = 496,
    LABEL = 497,
    LANGUAGE = 498,
    LARGE_P = 499,
    LAST_P = 500,
    LATERAL_P = 501,
    LEADING = 502,
    LEAKPROOF = 503,
    LEAST = 504,
    LEFT = 505,
    LEVEL = 506,
    LIKE = 507,
    LIMIT = 508,
    LISTEN = 509,
    LOAD = 510,
    LOCAL = 511,
    LOCALTIME = 512,
    LOCALTIMESTAMP = 513,
    LOCATION = 514,
    LOCK_P = 515,
    LOCKED = 516,
    LOGGED = 517,
    MAPPING = 518,
    MATCH = 519,
    MATCHED = 520,
    MATERIALIZED = 521,
    MAXVALUE = 522,
    MERGE = 523,
    MERGE_ACTION = 524,
    METHOD = 525,
    MINUTE_P = 526,
    MINVALUE = 527,
    MODE = 528,
    MONTH_P = 529,
    MOVE = 530,
    NAME_P = 531,
    NAMES = 532,
    NATIONAL = 533,
    NATURAL = 534,
    NCHAR = 535,
    NESTED = 536,
    NEW = 537,
    NEXT = 538,
    NFC = 539,
    NFD = 540,
    NFKC = 541,
    NFKD = 542,
    NO = 543,
    NONE = 544,
    NORMALIZE = 545,
    NORMALIZED = 546,
    NOT = 547,
    NOTHING = 548,
    NOTIFY = 549,
    NOTNULL = 550,
    NOWAIT = 551,
    NULL_P = 552,
    NULLIF = 553,
    NULLS_P = 554,
    NUMERIC = 555,
    OBJECT_P = 556,
    OF = 557,
    OFF = 558,
    OFFSET = 559,
    OIDS = 560,
    OLD = 561,
    OMIT = 562,
    ON = 563,
    ONLY = 564,
    OPERATOR = 565,
    OPTION = 566,
    OPTIONS = 567,
    OR = 568,
    ORDER = 569,
    ORDINALITY = 570,
    OTHERS = 571,
    OUT_P = 572,
    OUTER_P = 573,
    OVER = 574,
    OVERLAPS = 575,
    OVERLAY = 576,
    OVERRIDING = 577,
    OWNED = 578,
    OWNER = 579,
    PARALLEL = 580,
    PARAMETER = 581,
    PARSER = 582,
    PARTIAL = 583,
    PARTITION = 584,
    PASSING = 585,
    PASSWORD = 586,
    PATH = 587,
    PGPOOL = 588,
    PLACING = 589,
    PLAN = 590,
    PLANS = 591,
    POLICY = 592,
    POSITION = 593,
    PRECEDING = 594,
    PRECISION = 595,
    PRESERVE = 596,
    PREPARE = 597,
    PREPARED = 598,
    PRIMARY = 599,
    PRIOR = 600,
    PRIVILEGES = 601,
    PROCEDURAL = 602,
    PROCEDURE = 603,
    PROCEDURES = 604,
    PROGRAM = 605,
    PUBLICATION = 606,
    QUOTE = 607,
    QUOTES = 608,
    RANGE = 609,
    READ = 610,
    REAL = 611,
    REASSIGN = 612,
    RECHECK = 613,
    RECURSIVE = 614,
    REF_P = 615,
    REFERENCES = 616,
    REFERENCING = 617,
    REFRESH = 618,
    REINDEX = 619,
    RELATIVE_P = 620,
    RELEASE = 621,
    RENAME = 622,
    REPEATABLE = 623,
    REPLACE = 624,
    REPLICA = 625,
    RESET = 626,
    RESTART = 627,
    RESTRICT = 628,
    RETURN = 629,
    RETURNING = 630,
    RETURNS = 631,
    REVOKE = 632,
    RIGHT = 633,
    ROLE = 634,
    ROLLBACK = 635,
    ROLLUP = 636,
    ROUTINE = 637,
    ROUTINES = 638,
    ROW = 639,
    ROWS = 640,
    RULE = 641,
    SAVEPOINT = 642,
    SCALAR = 643,
    SCHEMA = 644,
    SCHEMAS = 645,
    SCROLL = 646,
    SEARCH = 647,
    SECOND_P = 648,
    SECURITY = 649,
    SELECT = 650,
    SEQUENCE = 651,
    SEQUENCES = 652,
    SERIALIZABLE = 653,
    SERVER = 654,
    SESSION = 655,
    SESSION_USER = 656,
    SET = 657,
    SETS = 658,
    SETOF = 659,
    SHARE = 660,
    SHOW = 661,
    SIMILAR = 662,
    SIMPLE = 663,
    SKIP = 664,
    SMALLINT = 665,
    SNAPSHOT = 666,
    SOME = 667,
    SOURCE = 668,
    SQL_P = 669,
    STABLE = 670,
    STANDALONE_P = 671,
    START = 672,
    STATEMENT = 673,
    STATISTICS = 674,
    STDIN = 675,
    STDOUT = 676,
    STORAGE = 677,
    STORED = 678,
    STRICT_P = 679,
    STRING_P = 680,
    STRIP_P = 681,
    SUBSCRIPTION = 682,
    SUBSTRING = 683,
    SUPPORT = 684,
    SYMMETRIC = 685,
    SYSID = 686,
    SYSTEM_P = 687,
    SYSTEM_USER = 688,
    TABLE = 689,
    TABLES = 690,
    TABLESAMPLE = 691,
    TABLESPACE = 692,
    TARGET = 693,
    TEMP = 694,
    TEMPLATE = 695,
    TEMPORARY = 696,
    TEXT_P = 697,
    THEN = 698,
    TIES = 699,
    TIME = 700,
    TIMESTAMP = 701,
    TO = 702,
    TRAILING = 703,
    TRANSACTION = 704,
    TRANSFORM = 705,
    TREAT = 706,
    TRIGGER = 707,
    TRIM = 708,
    TRUE_P = 709,
    TRUNCATE = 710,
    TRUSTED = 711,
    TYPE_P = 712,
    TYPES_P = 713,
    UESCAPE = 714,
    UNBOUNDED = 715,
    UNCONDITIONAL = 716,
    UNCOMMITTED = 717,
    UNENCRYPTED = 718,
    UNION = 719,
    UNIQUE = 720,
    UNKNOWN = 721,
    UNLISTEN = 722,
    UNLOGGED = 723,
    UNTIL = 724,
    UPDATE = 725,
    USER = 726,
    USING = 727,
    VACUUM = 728,
    VALID = 729,
    VALIDATE = 730,
    VALIDATOR = 731,
    VALUE_P = 732,
    VALUES = 733,
    VARCHAR = 734,
    VARIADIC = 735,
    VARYING = 736,
    VERBOSE = 737,
    VERSION_P = 738,
    VIEW = 739,
    VIEWS = 740,
    VOLATILE = 741,
    WHEN = 742,
    WHERE = 743,
    WHITESPACE_P = 744,
    WINDOW = 745,
    WITH = 746,
    WITHIN = 747,
    WITHOUT = 748,
    WORK = 749,
    WRAPPER = 750,
    WRITE = 751,
    XML_P = 752,
    XMLATTRIBUTES = 753,
    XMLCONCAT = 754,
    XMLELEMENT = 755,
    XMLEXISTS = 756,
    XMLFOREST = 757,
    XMLNAMESPACES = 758,
    XMLPARSE = 759,
    XMLPI = 760,
    XMLROOT = 761,
    XMLSERIALIZE = 762,
    XMLTABLE = 763,
    YEAR_P = 764,
    YES_P = 765,
    ZONE = 766,
    FORMAT_LA = 767,
    NOT_LA = 768,
    NULLS_LA = 769,
    WITH_LA = 770,
    WITHOUT_LA = 771,
    MODE_TYPE_NAME = 772,
    MODE_PLPGSQL_EXPR = 773,
    MODE_PLPGSQL_ASSIGN1 = 774,
    MODE_PLPGSQL_ASSIGN2 = 775,
    MODE_PLPGSQL_ASSIGN3 = 776,
    UMINUS = 777
  };
#endif
/* Tokens.  */
#define IDENT 258
#define UIDENT 259
#define FCONST 260
#define SCONST 261
#define USCONST 262
#define BCONST 263
#define XCONST 264
#define Op 265
#define ICONST 266
#define PARAM 267
#define TYPECAST 268
#define DOT_DOT 269
#define COLON_EQUALS 270
#define EQUALS_GREATER 271
#define LESS_EQUALS 272
#define GREATER_EQUALS 273
#define NOT_EQUALS 274
#define ABORT_P 275
#define ABSENT 276
#define ABSOLUTE_P 277
#define ACCESS 278
#define ACTION 279
#define ADD_P 280
#define ADMIN 281
#define AFTER 282
#define AGGREGATE 283
#define ALL 284
#define ALSO 285
#define ALTER 286
#define ALWAYS 287
#define ANALYSE 288
#define ANALYZE 289
#define AND 290
#define ANY 291
#define ARRAY 292
#define AS 293
#define ASC 294
#define ASENSITIVE 295
#define ASSERTION 296
#define ASSIGNMENT 297
#define ASYMMETRIC 298
#define ATOMIC 299
#define AT 300
#define ATTACH 301
#define ATTRIBUTE 302
#define AUTHORIZATION 303
#define BACKWARD 304
#define BEFORE 305
#define BEGIN_P 306
#define BETWEEN 307
#define BIGINT 308
#define BINARY 309
#define BIT 310
#define BOOLEAN_P 311
#define BOTH 312
#define BREADTH 313
#define BY 314
#define CACHE 315
#define CALL 316
#define CALLED 317
#define CASCADE 318
#define CASCADED 319
#define CASE 320
#define CAST 321
#define CATALOG_P 322
#define CHAIN 323
#define CHAR_P 324
#define CHARACTER 325
#define CHARACTERISTICS 326
#define CHECK 327
#define CHECKPOINT 328
#define CLASS 329
#define CLOSE 330
#define CLUSTER 331
#define COALESCE 332
#define COLLATE 333
#define COLLATION 334
#define COLUMN 335
#define COLUMNS 336
#define COMMENT 337
#define COMMENTS 338
#define COMMIT 339
#define COMMITTED 340
#define COMPRESSION 341
#define CONCURRENTLY 342
#define CONDITIONAL 343
#define CONFIGURATION 344
#define CONFLICT 345
#define CONNECTION 346
#define CONSTRAINT 347
#define CONSTRAINTS 348
#define CONTENT_P 349
#define CONTINUE_P 350
#define CONVERSION_P 351
#define COPY 352
#define COST 353
#define CREATE 354
#define CROSS 355
#define CSV 356
#define CUBE 357
#define CURRENT_P 358
#define CURRENT_CATALOG 359
#define CURRENT_DATE 360
#define CURRENT_ROLE 361
#define CURRENT_SCHEMA 362
#define CURRENT_TIME 363
#define CURRENT_TIMESTAMP 364
#define CURRENT_USER 365
#define CURSOR 366
#define CYCLE 367
#define DATA_P 368
#define DATABASE 369
#define DAY_P 370
#define DEALLOCATE 371
#define DEC 372
#define DECIMAL_P 373
#define DECLARE 374
#define DEFAULT 375
#define DEFAULTS 376
#define DEFERRABLE 377
#define DEFERRED 378
#define DEFINER 379
#define DELETE_P 380
#define DELIMITER 381
#define DELIMITERS 382
#define DEPENDS 383
#define DEPTH 384
#define DESC 385
#define DETACH 386
#define DICTIONARY 387
#define DISABLE_P 388
#define DISCARD 389
#define DISTINCT 390
#define DO 391
#define DOCUMENT_P 392
#define DOMAIN_P 393
#define DOUBLE_P 394
#define DROP 395
#define EACH 396
#define ELSE 397
#define EMPTY_P 398
#define ENABLE_P 399
#define ENCODING 400
#define ENCRYPTED 401
#define END_P 402
#define ENUM_P 403
#define ERROR_P 404
#define ESCAPE 405
#define EVENT 406
#define EXCEPT 407
#define EXCLUDE 408
#define EXCLUDING 409
#define EXCLUSIVE 410
#define EXECUTE 411
#define EXISTS 412
#define EXPLAIN 413
#define EXPRESSION 414
#define EXTENSION 415
#define EXTERNAL 416
#define EXTRACT 417
#define FALSE_P 418
#define FAMILY 419
#define FETCH 420
#define FILTER 421
#define FINALIZE 422
#define FIRST_P 423
#define FLOAT_P 424
#define FOLLOWING 425
#define FOR 426
#define FORCE 427
#define FOREIGN 428
#define FORMAT 429
#define FORWARD 430
#define FREEZE 431
#define FROM 432
#define FULL 433
#define FUNCTION 434
#define FUNCTIONS 435
#define GENERATED 436
#define GLOBAL 437
#define GRANT 438
#define GRANTED 439
#define GREATEST 440
#define GROUP_P 441
#define GROUPING 442
#define GROUPS 443
#define HANDLER 444
#define HAVING 445
#define HEADER_P 446
#define HOLD 447
#define HOUR_P 448
#define IDENTITY_P 449
#define IF_P 450
#define ILIKE 451
#define IMMEDIATE 452
#define IMMUTABLE 453
#define IMPLICIT_P 454
#define IMPORT_P 455
#define IN_P 456
#define INCLUDE 457
#define INCLUDING 458
#define INCREMENT 459
#define INDENT 460
#define INDEX 461
#define INDEXES 462
#define INHERIT 463
#define INHERITS 464
#define INITIALLY 465
#define INLINE_P 466
#define INNER_P 467
#define INOUT 468
#define INPUT_P 469
#define INSENSITIVE 470
#define INSERT 471
#define INSTEAD 472
#define INT_P 473
#define INTEGER 474
#define INTERSECT 475
#define INTERVAL 476
#define INTO 477
#define INVOKER 478
#define IS 479
#define ISNULL 480
#define ISOLATION 481
#define JOIN 482
#define JSON 483
#define JSON_ARRAY 484
#define JSON_ARRAYAGG 485
#define JSON_EXISTS 486
#define JSON_OBJECT 487
#define JSON_OBJECTAGG 488
#define JSON_QUERY 489
#define JSON_SCALAR 490
#define JSON_SERIALIZE 491
#define JSON_TABLE 492
#define JSON_VALUE 493
#define KEEP 494
#define KEY 495
#define KEYS 496
#define LABEL 497
#define LANGUAGE 498
#define LARGE_P 499
#define LAST_P 500
#define LATERAL_P 501
#define LEADING 502
#define LEAKPROOF 503
#define LEAST 504
#define LEFT 505
#define LEVEL 506
#define LIKE 507
#define LIMIT 508
#define LISTEN 509
#define LOAD 510
#define LOCAL 511
#define LOCALTIME 512
#define LOCALTIMESTAMP 513
#define LOCATION 514
#define LOCK_P 515
#define LOCKED 516
#define LOGGED 517
#define MAPPING 518
#define MATCH 519
#define MATCHED 520
#define MATERIALIZED 521
#define MAXVALUE 522
#define MERGE 523
#define MERGE_ACTION 524
#define METHOD 525
#define MINUTE_P 526
#define MINVALUE 527
#define MODE 528
#define MONTH_P 529
#define MOVE 530
#define NAME_P 531
#define NAMES 532
#define NATIONAL 533
#define NATURAL 534
#define NCHAR 535
#define NESTED 536
#define NEW 537
#define NEXT 538
#define NFC 539
#define NFD 540
#define NFKC 541
#define NFKD 542
#define NO 543
#define NONE 544
#define NORMALIZE 545
#define NORMALIZED 546
#define NOT 547
#define NOTHING 548
#define NOTIFY 549
#define NOTNULL 550
#define NOWAIT 551
#define NULL_P 552
#define NULLIF 553
#define NULLS_P 554
#define NUMERIC 555
#define OBJECT_P 556
#define OF 557
#define OFF 558
#define OFFSET 559
#define OIDS 560
#define OLD 561
#define OMIT 562
#define ON 563
#define ONLY 564
#define OPERATOR 565
#define OPTION 566
#define OPTIONS 567
#define OR 568
#define ORDER 569
#define ORDINALITY 570
#define OTHERS 571
#define OUT_P 572
#define OUTER_P 573
#define OVER 574
#define OVERLAPS 575
#define OVERLAY 576
#define OVERRIDING 577
#define OWNED 578
#define OWNER 579
#define PARALLEL 580
#define PARAMETER 581
#define PARSER 582
#define PARTIAL 583
#define PARTITION 584
#define PASSING 585
#define PASSWORD 586
#define PATH 587
#define PGPOOL 588
#define PLACING 589
#define PLAN 590
#define PLANS 591
#define POLICY 592
#define POSITION 593
#define PRECEDING 594
#define PRECISION 595
#define PRESERVE 596
#define PREPARE 597
#define PREPARED 598
#define PRIMARY 599
#define PRIOR 600
#define PRIVILEGES 601
#define PROCEDURAL 602
#define PROCEDURE 603
#define PROCEDURES 604
#define PROGRAM 605
#define PUBLICATION 606
#define QUOTE 607
#define QUOTES 608
#define RANGE 609
#define READ 610
#define REAL 611
#define REASSIGN 612
#define RECHECK 613
#define RECURSIVE 614
#define REF_P 615
#define REFERENCES 616
#define REFERENCING 617
#define REFRESH 618
#define REINDEX 619
#define RELATIVE_P 620
#define RELEASE 621
#define RENAME 622
#define REPEATABLE 623
#define REPLACE 624
#define REPLICA 625
#define RESET 626
#define RESTART 627
#define RESTRICT 628
#define RETURN 629
#define RETURNING 630
#define RETURNS 631
#define REVOKE 632
#define RIGHT 633
#define ROLE 634
#define ROLLBACK 635
#define ROLLUP 636
#define ROUTINE 637
#define ROUTINES 638
#define ROW 639
#define ROWS 640
#define RULE 641
#define SAVEPOINT 642
#define SCALAR 643
#define SCHEMA 644
#define SCHEMAS 645
#define SCROLL 646
#define SEARCH 647
#define SECOND_P 648
#define SECURITY 649
#define SELECT 650
#define SEQUENCE 651
#define SEQUENCES 652
#define SERIALIZABLE 653
#define SERVER 654
#define SESSION 655
#define SESSION_USER 656
#define SET 657
#define SETS 658
#define SETOF 659
#define SHARE 660
#define SHOW 661
#define SIMILAR 662
#define SIMPLE 663
#define SKIP 664
#define SMALLINT 665
#define SNAPSHOT 666
#define SOME 667
#define SOURCE 668
#define SQL_P 669
#define STABLE 670
#define STANDALONE_P 671
#define START 672
#define STATEMENT 673
#define STATISTICS 674
#define STDIN 675
#define STDOUT 676
#define STORAGE 677
#define STORED 678
#define STRICT_P 679
#define STRING_P 680
#define STRIP_P 681
#define SUBSCRIPTION 682
#define SUBSTRING 683
#define SUPPORT 684
#define SYMMETRIC 685
#define SYSID 686
#define SYSTEM_P 687
#define SYSTEM_USER 688
#define TABLE 689
#define TABLES 690
#define TABLESAMPLE 691
#define TABLESPACE 692
#define TARGET 693
#define TEMP 694
#define TEMPLATE 695
#define TEMPORARY 696
#define TEXT_P 697
#define THEN 698
#define TIES 699
#define TIME 700
#define TIMESTAMP 701
#define TO 702
#define TRAILING 703
#define TRANSACTION 704
#define TRANSFORM 705
#define TREAT 706
#define TRIGGER 707
#define TRIM 708
#define TRUE_P 709
#define TRUNCATE 710
#define TRUSTED 711
#define TYPE_P 712
#define TYPES_P 713
#define UESCAPE 714
#define UNBOUNDED 715
#define UNCONDITIONAL 716
#define UNCOMMITTED 717
#define UNENCRYPTED 718
#define UNION 719
#define UNIQUE 720
#define UNKNOWN 721
#define UNLISTEN 722
#define UNLOGGED 723
#define UNTIL 724
#define UPDATE 725
#define USER 726
#define USING 727
#define VACUUM 728
#define VALID 729
#define VALIDATE 730
#define VALIDATOR 731
#define VALUE_P 732
#define VALUES 733
#define VARCHAR 734
#define VARIADIC 735
#define VARYING 736
#define VERBOSE 737
#define VERSION_P 738
#define VIEW 739
#define VIEWS 740
#define VOLATILE 741
#define WHEN 742
#define WHERE 743
#define WHITESPACE_P 744
#define WINDOW 745
#define WITH 746
#define WITHIN 747
#define WITHOUT 748
#define WORK 749
#define WRAPPER 750
#define WRITE 751
#define XML_P 752
#define XMLATTRIBUTES 753
#define XMLCONCAT 754
#define XMLELEMENT 755
#define XMLEXISTS 756
#define XMLFOREST 757
#define XMLNAMESPACES 758
#define XMLPARSE 759
#define XMLPI 760
#define XMLROOT 761
#define XMLSERIALIZE 762
#define XMLTABLE 763
#define YEAR_P 764
#define YES_P 765
#define ZONE 766
#define FORMAT_LA 767
#define NOT_LA 768
#define NULLS_LA 769
#define WITH_LA 770
#define WITHOUT_LA 771
#define MODE_TYPE_NAME 772
#define MODE_PLPGSQL_EXPR 773
#define MODE_PLPGSQL_ASSIGN1 774
#define MODE_PLPGSQL_ASSIGN2 775
#define MODE_PLPGSQL_ASSIGN3 776
#define UMINUS 777

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED

union YYSTYPE
{
#line 263 "gram_minimal.y" /* yacc.c:1909  */

	core_YYSTYPE core_yystype;
	/* these fields must match core_YYSTYPE: */
	int			ival;
	char	   *str;
	const char *keyword;

	char		chr;
	bool		boolean;
	JoinType	jtype;
	DropBehavior dbehavior;
	OnCommitAction oncommit;
	List	   *list;
	Node	   *node;
	ObjectType	objtype;
	TypeName   *typnam;
	FunctionParameter *fun_param;
	FunctionParameterMode fun_param_mode;
	ObjectWithArgs *objwithargs;
	DefElem	   *defelt;
	SortBy	   *sortby;
	WindowDef  *windef;
	JoinExpr   *jexpr;
	IndexElem  *ielem;
	StatsElem  *selem;
	Alias	   *alias;
	RangeVar   *range;
	IntoClause *into;
	WithClause *with;
	InferClause	*infer;
	OnConflictClause *onconflict;
	A_Indices  *aind;
	ResTarget  *target;
	struct PrivTarget *privtarget;
	AccessPriv *accesspriv;
	struct ImportQual *importqual;
	InsertStmt *istmt;
	VariableSetStmt *vsetstmt;
	PartitionElem *partelem;
	PartitionSpec *partspec;
	PartitionBoundSpec *partboundspec;
	RoleSpec   *rolespec;
	PublicationObjSpec *publicationobjectspec;
	struct SelectLimit *selectlimit;
	SetQuantifier setquantifier;
	struct GroupClause *groupclause;
	MergeMatchKind mergematch;
	MergeWhenClause *mergewhen;
	struct KeyActions *keyactions;
	struct KeyAction *keyaction;

#line 1150 "gram_minimal.h" /* yacc.c:1909  */
};

typedef union YYSTYPE YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE YYLTYPE;
struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif



int minimal_base_yyparse (core_yyscan_t yyscanner);

#endif /* !YY_MINIMAL_BASE_YY_GRAM_MINIMAL_H_INCLUDED  */
